# Logo3D interpreter

Logo3D is a simple language that allows the user, with the usage of
a 3D turtle, to draw in a 3D space with a set of predefined functions.
It can also be treated as a language for coding simple programms.

With the implementation of the iterpreter the user can perform many
interesting features that will be described later on this docuement.

It's also important to comment that there is only one defined type. This type is
the real numbers like ```5.4863```, ```.54863```, ```54863```.

## Installation

First of all install python:

```bash
sudo apt install python3.8
```
Check the version installed with ```python3 --version```. Should be
Python 3.8.5 or higher. Then install the package manager pip3.

```bash
sudo apt install python3-pip
```

Procede downloading antlr4.
You can follow [this](https://github.com/antlr/antlr4/blob/master/doc/getting-started.md) instructions.

Finally install all the requirements that code need (antlr4-runtime and vpython) with:

```bash
python3 -m pip install -r requirements.txt
```

## Usage

To start using this logo3D interpreter you will need to
create all the necessary grammar files with:

```bash
antlr4 -Dlanguage=Python3 -no-listener -visitor logo3d.g
```

Then, you will need to create a programm like ```yourProgramm.l3d```
which will contain the programm you want to execute.

Once your programm is ready you can just simply execute:

```bash
python3 logo3d.py yourProgramm.l3d
```

This command will interpret you programm and execute the
```main``` function by default. But, you can use extra parameters
to start the execution by any defined function of your programm.
This can be achieved by giving it's name and the needed real parameters.
Check this example starting at function ```foo(a)``` which requires
one parameter to execute:

```bash
python3 logo3d.py yourProgramm.l3d foo 5
```
This will start the programm by executing the function ```foo``` with the given parameter ```5```.

When the execution finishes a message like: ```Execution of function foo finished correctly``` 
will be displayed. If there is a return value in the first function (foo in this case) this will 
be also shown when the execution finishes.

## Turtle3D
In order to be able to draw in a 3D space I implemented a class ```turtle3d``` (that can be found
at ```turtle3d.py```) which is capable of doing this kind of stuff. It has a simple but
powerful set of functions that will allow you to draw amazing figures or scenes.

The available functions are the following ones:

- ```forward(x)```: 
    Function that moves (painting) the turtle ```x``` units forward.

- ```backward(x)```:
    Function that moves (painting) the turtle ```x``` units forward.

- ```right(x)```:
    Function that moves ```x``` degrees the looking direction of the turtle to the right.

- ```left(x)```:
    Function that moves ```x``` degrees the looking direction of the turtle to the left.

- ```up(x)```:
    Function that moves ```x``` degrees the looking direction of the turtle up.

- ```down(x)```:
    Function that moves ```x``` degrees the looking direction of the turtle down.

- ```color(r,g,b)```:
    Function that changes the color of the turtle with the new ```r```, ```g```, ```b``` given.

- ```hide()```:
    Function that hides the turtle.

- ```show()```:
    Function that make the turtle visible.

- ```home()```:
    Function that moves without painting the turtle to it's home coordinates ```(0,0,0)```.

- ```help()```:
    Function that shows the usage of all the functions described above.
    
Any of those functions return anything. They only modify the implicit
turtle which can draw in an localhost automatically displayed.

If no function that draw is used (```forward(x)``` or ```backward(x)```) 
the graphic window will not be displayed.

## Extensions added

Before I start talking about how I structured the grammar I will introduce the extensions
I did to the language in order to understand it's true potential. Every
extension has it's own simple testing example located in the ```./test/``` folder.

### Else
I implement the recognition and correct evaluation of an ```else``` after
an ```if```. This will allow conditional sentences to be more expressive like, 
per example, having a cascade of else-if conditions.

Testing example ```test-else.l3d```.

---

### Operators added

All the following operators precedences were respected and implemented using
[this](https://www.programiz.com/python-programming/precedence-associativity)
guide. Even thought the correct presedence order will be explicitly explained in the Grammar structure section.

There are quite a lot so I only did one test example with the usage of
all of them at once to check their correctness.

Testing example ```test-op.l3d```.

#### Integer division
Symbol ``` \ ``` will represent the integer division operator.

Example: ```5 \ 2``` will be equal to ```2```.

It's behaviour is like a floor operator applied after the division.

#### Modulo
Symbol ```%``` will represent the modulo operator.

Example: ```10 % 3``` will be equal to ```1```.

The modulo operator is equal to the residue of the integer division.

#### Logical operators
Added the logical operators ```AND```,```OR``` and ```NOT```.

Examples: ```(1 AND 0) OR (NOT 9)``` will be equal to ```0```
but ```(1 AND 0) OR (NOT 0)``` will be equal to ```1```.

The ```NOT``` operator has a higher presedence than ```AND``` which has a higher precedence than ```OR```.

Remember that a value being inside ```[-1e-6, 1e-6]``` is considered 0 (false)
and every other value is considered 1 (true).

#### Unary + and - operators
Unary ```+``` and ```-``` operators supported.

Examples: ```+(10 - 3)``` will be equal to ```7``` and ```-(10 - 3)``` will be equal to ```-7```.

Those simple operators are really necessary because I didn't let the real numbers to be negative.
Once I notice this fact I realised that an expression like ```(4*5+4/7)``` could be also have
the operator ```+``` or ```-``` at the start so I decided to use this idea to create these operators
that will implicitly allow negative real numbers like ```-42.23```.

#### Auto-increment and auto-decrement
Auto-increment ```++``` and auto-decrement ```--``` before a variable added.

Example: If ```x := 12``` then ```<< ++x``` will print ```13``` and variable ```x``` will be equal to ```13```.
```--x``` will have the same behaviour but printing and storing ```11``` instead.

These operators (```++``` and ```--```) can only be applied upon variables and they increment or decrement it's stored value by 1
respectively. You should be careful with the unary operators exaplained before. It's recommended
to use parenthesis if some of this operators and the previous ones are being mixed 
like ```---x``` will cause problems.

---

### Incrementing and decrementing assignament
Incrementing ```+=``` and decrementing ```-=``` assignament implemented.

It's behaviour is the classical one. Those assignament operators can be traduced as:
```x += 5``` ==  ```x = x + 5``` and ```x -= 5``` ==  ```x = x - 5```.
Obviously, only variables can be assigned. 
The value to be asigned can be the result of a function or any expression.

Example: If ```x := 15``` then ```x += 25``` will store 15 + 25 = ```40``` in the variable ```x```.

```x -= fact(4)``` will have the same behaviour but storing ```-9``` instead.

Testing example ```test-incdec.l3d```.

---

### Upper/Lower case
All key words can be written in upper or lower case.

Example: ```IF == if```, ```PROC == proc```, ```END == end```, etc.

Testing example ```test-case.l3d```.

---

### Block comment
Block comments can be written like ```/* ... */```.

Example: ```/* This is a block comment */```.

Testing example ```test-block.l3d```.

---

### Returns
This is the main change I apply to the interpreter.
I changed it to let it accept ```RETURN expression``` instructions.
It can be followed by any expression or can be only used to finish a computation returning *void*.
The returned values can be saved, written, used in another expression, etc.
Any time a ```RETURN``` is used the procedure who did it will stop it's execution and return
the value or expression that may follow him.

Examples where returning an expression:

*Factorial*
```
PROC fact(n) IS
    IF n == 0 THEN
        RETURN 1
    ELSE
        RETURN n * fact(n - 1)
    END
END
```
*Fibonacci*
```
PROC fib(n) IS
    IF n <= 1 THEN
        RETURN n
    ELSE
        RETURN fib(n - 1) + fib(n - 2)
    END
END
```
Example where returning to finish computation:

*Sleep function*
```
PROC sleep(n) IS
    i := 0
    WHILE 1 DO
        IF i == n THEN RETURN END
        i := i + 1 
    END
END
```

Testing example ```test-ret.l3d```.

---

### Switch
The typical switch instruction can be used and works as expected.
It has the following syntaxis:

```SWITCH (expression) cases defaultCase END```

where cases can have many or any cases like:
```
CASE .4 (statements)
CASE 46.73 (statements)
CASE 8 (statements)
...
```
and defaultCase may have have or not a default behaviour if no BREAK is achieved.
```
DEFAULT (statements)
```
I also added the instruction BREAK that works exactly as a c++ break. It will stop at the end of a case and 
didn't let the other cases to match. If any break is added the code will continue normally
searching for the next match or, eventually if defined, the default case.

As expected, the execution of the entire switch will finish if a ```RETURN``` instruction is used.

Notice that the default case is not compulsory in this implementation. I decided to do it like this
because if there is no match between any target case the switch can simply do nothing. And we will
continue by the next statement.

Testing example ```test-switch.l3d```.

---

### Write text + endl
I also implemented a way to print messagges to the console standard output.
This implementation allow putting text in the ```<<``` command that firstly only can print numbers.
Strings must be written like ```"this is a string example"```. Even thought, these string
cannot contain ```\n``` directive or these type of commands with the usage of ``` \ ```.

To supply these lack of expresiveness I implemented the ENDL keyword which will print
an end of line whenever used. It must be used alone (without any number or string) 
following the next syntax: ```<< endl``` or ```<< ENDL```.

No string and number can be mixed nor concatenated together in the same writting statement.

Example: ```<< "The result of the factorial of 13 is " << fact(13) << endl```

Testing example ```test-string.l3d```.

---

### Error handeling
To make this interpreter a little bit better I implemented some error handeling
for some kind of errors.
The interpreter will throw an error when:
- Incorrect grammar syntax
    - This error will be shown whenever there is a syntax error in the programm (e.g. ```IF x < 3 THN ...```)
    - Even thought the execution of the programm will continue the best way it could
    as I didn't implement any error visitors.
- Not declared procedure
    - This error will be shown whenever there is a call to a procedure that didn't have been declared.
- Procedure redeclaration
    - This error will be shown whenever two or more procedures with the same name are being declared.
- Invalid number of arguments in any procedure call 
    - This error will be shown whenever a procedure is called with an incorrect number of parameters (e.g. ```fact(1,2)``` if fact expect one parameter)
    - If the function is a Turtle3D function an usage message for the function will be shown.
- Invalid procedure name
    - This error will be shown whenever a Turtle3D function name (such as ```forward```) is used as a procedure name (e.g. ```PROC up() IS```)
    - So, no procedures with the name of a turtle3d function can be declared 
- Division by 0 is performed
    - This error will be shown whenever a division, integer division or modulo is produced with a 0 
    (e.g. ```5/0```, ```5\(2-2)```, ```5%(2*0)```)
- Not returned value for procedure call that needs to be evaluated
    - This error will be shown whenever there is an expected returned value by a function which didn't
    return anything or return *void* 
    - (e.g. ```PROC sayHello IS << "Hello" << ENDL END``` used in ```<< 5+sayHello()*3```) 

Testing example ```test-error.l3d```. You can comment or uncomment some lines to obtain different types
of errors.

## Grammar structure

Logo3D is a really simple language so I created a simple grammar to recognize it.
The most important idea is that a Logo3D programmm is only a bunch of procedures
(as many other languages). Procedures in Logo3D are described sintaxically as follows:

```PROC functionName(arguments) IS statements END```

Those procedures can call any other procedure, even themselves recursively, or turtle3d functions.
They allow a variable number of parameters and they must be separated by commas. If there is no
paramethers must be written like: ```foo()```. These arguments can only be variables and no
default value can be given to them.

Then we can locate 2 important parts: expressions and statements.

### Expression
An **expression** could be (in precedence order respecting
[this](https://www.programiz.com/python-programming/precedence-associativity)
guide):
- Parenthesized (e.g. ```(5 + 2)```)
    - Any kind of expression can be encapsulated between parenthesis and
    it's priority will be the greatest.
- Unary +/- operators (e.g. ```+(5 + 2)``` or ```-(5 + 2)```)
    - This operators can be used with any other expression like ```-X``` or ```-1.468 + 2.4```
- Auto-increment and auto-decrement (e.g. ```++x``` or ```--A```)
    - This operators can only be used with variables as long as they have to store the
    modified value somewhere.
- Arithmetic high precedence operators:
    - Multiplication (e.g. ```5 * 2```)
    - Division (e.g. ```5 / 2```)
    - Modulo (e.g. ```5 % 2```)
    - Integer division (e.g. ```5 \ 2```)
        - All of these operators are binary and, hence, require two correct expressions to operate with.
        - Last three operators can't divide by 0 so an error message will be thrown in this case.
- Arithmetic low precedence operators:   
    - Addition (e.g. ```5 + 2```)
    - Substraction (e.g. ```5 - 2```)
        - As the previous expressions these are also binary operators thath require two correct expressions.
- Relational operators:
    - Equal (e.g. ```2 == 2``` will be equal to ```1.0```)
    - Not equal (e.g. ```2 != 2``` will be equal to ```0.0```)
    - Greater than (e.g. ```4 > 2``` will be equal to ```1.0```)
    - Greater than or equal (e.g. ```2 >= 2``` will be equal to ```1.0```)
    - Less than (e.g. ```4 < 2``` will be equal to ```0.0```)
    - Less than or equal (e.g. ```2 <= 2``` will be equal to ```1.0```)
        - Remember that a value being inside ```[-1e-6, 1e-6]``` is considered 0 (false)
        and every other value is considered 1 (true).
- Logical operators:
    - Negation (e.g. ```NOT 0``` will be equal to ```1```)
    - Conjuction (e.g. ```0 AND 1``` will be equal to ```0```)
    - Disjuction (e.g. ```0 OR 1``` will be equal to ```1```)
        - Remember that a value being inside ```[-1e-6, 1e-6]``` is considered 0 (false)
        and every other value is considered 1 (true).
        - (e.g. ```1e-6 AND 1``` will be ```0.0```)
- Real number (e.g. ```5.246``` or ```.246``` or ```524```)
    - Real numbers can be used as usual. ```.134``` is the same as ```0.134```.
    - There are no negative numbers because the unary operator ```-``` can be used instead for all expressions.
    - No exponent numbers implemented in this language (e.g. ```1e-6``` won't be recognized).
- Variable (e.g. ```x``` or ```A```)
    - Variables didn't need to be initialized. If they aren't their default value will be ```0.0```.
    - Variables are local for each procedure so if you try to access to a variable in another scope
    you will be creating a fresh new variable that will be initialized to 0.
- A procedure call that return a value (e.g. ```fib(5)```)
    - This procedure call must return a value. 
    - Returning *void* is not considered a value. 
        - If those things didn't acomplish an error will be thrown and the execution will be terminated
        (see more information in the error handeling section).

### Statement
An **statement** could be:
- An asssignment
    - Normal assignament (e.g. ```x := 5 + 2```)
    - Incrementing assignament (e.g. ```x += 5 + 2```)
    - Decrementing assignament (e.g. ```x -= 5 + 2```)
        - All of these operators can have any type of expression in the right side like ```x += fact(5)```
- A reading (e.g. ```>> x```)
    - This action will interrupt the programm and *ask* the user to insert a number that will be saved in the variable 
        - ```>>``` operand must be followed by a variable which will store the input value
- A writing
    - This action will write the desired object on the standard output
    - It can write:
        - Expressions (e.g. ```<< 5 + 2``` or ```<< fact(3)```)
        - Strings (e.g. ```<< "Example"```)
        - End of lines (e.g. ```<< endl```)
- A conditional 
    - This construction is capable of interpreting an if-else conditional.
    It's important to see that ```ELSE``` is not compulsory.
    If the condition is satisfied (true) the code that came after the ```THEN``` 
    key-word will be executed till ```ELSE``` or ```END``` is found.
    If the condition is **not** satisfied (false) and the code has an ```ELSE``` section,
    this will be the section that will be executed.
    - (e.g. ```IF x > 5 THEN (<< 6 << 5 + 2) ELSE << 7 + 8 END```)
- A while loop 
    - This construction allows the user to manage ```while``` loops.
    While the condition is satisfied (true) the code inside the ```DO``` section will be executed
    repeatedly. It will only finish when the condition is not any more satisfied or a ```RETURN```
    statement is used.
    - (e.g. ```WHILE x < 10 DO x:=x + 1 END```)
- A for loop
    - This construction allows the user to manage ```for``` loops.
    This ```for``` construction will allow the user to use an iterative way of working step by step. Per default
    the ```for``` will automatically increment the initial variable by a unit. Even thought, the user is capable
    of modifying this variable inside the code of the loop. This iterative construction will execute all
    it's code repeatedly while the variable is small or equal to the ```TO``` expression.
    It may also finsih before if a ```RETURN``` statement is used.
    - (e.g. ```FOR i FROM 0 TO 10 DO << i END```)
- A switch case 
    - This extra construction will allow the user to manage ```switch``` cases conditions.
    This construction is exaplained in detail in the extensions section. 
    - (e.g. 
        ```
        SWITCH (x) 
            CASE 1 << "Case with 1" << endl 
            CASE 2 << "Case with 2" << endl 
            DEFAULT << "Decault case" << endl
        END
        ```
        )
- A procedure call 
    - This statement will allow the user to call procedures in any moment of the execution.
    This calls can be made to the same procedure as long as we are able to deal with recursion.
    - If the number of parameters is not correct an error will stop the execution informing the user
    (see more information in the error handeling section).
    - This procedure calls can or not return a value, it won't matter because they are not used in any expression.
    - Procedures will continue their execution when the procedure call finish it's execution
    by completing all the instructions or because a ```RETURN``` instruction was used.
    - (e.g. ```foo(5 + 2)```)
- A return (e.g. ```RETURN 5 + 2``` or ```RETURN```)
    - This statement will finish the execution of any procedure when found.
    - It is exaplained in more detail in the extensions section. 
    
To finalize just mention that any identation, blank space or carriage return
are being skipped as long as this language is not whitespace-sensitive.