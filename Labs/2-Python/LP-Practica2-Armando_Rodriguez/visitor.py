import sys

from turtle3d import Turtle3D

if __name__ is not None and "." in __name__:
    from .logo3dParser import logo3dParser
    from .logo3dVisitor import logo3dVisitor
else:
    from logo3dParser import logo3dParser
    from logo3dVisitor import logo3dVisitor

class visitor(logo3dVisitor):
    # Function or procedure dictionary.
    functionsDict = {}
    # Indicates if we achieve a return instruction (we should stop a loop or the entire execution).
    returning = False
    # The value that is being returned. If void is returned, valueReturned will continue having None.
    valueReturned = None
    
    # Default constructor that create the dictionary, the stack of dictionarys to deal with different procedures context and the turtle.
    def __init__(self):
        self.symbolStack = []
        self.symbolDict = {}
        self.turtle = Turtle3D()

    # Static private method that prints and error and exit the execution of the programm.
    @staticmethod
    def __errorandexit(text):
        s = "\n*********************"
        for _ in text: s += "*"
        s += "\n* Execution error: " + text + " *\n*********************"
        for _ in text: s += "*"
        print(s)
        sys.exit(1)
    
    # Function that executes the procedure determined by the entering parameter.
    # The first element of the variable arguments is the name of the function and, if there are some parameters
    # they will be the rest of the elements
    def run(self, arguments):
        functionName = arguments[0]

        # We search for the procedure in the function dictionary throwing an error if the
        # desired procedure is not declared.
        if functionName not in self.functionsDict:
            self.__errorandexit("No procedure " + functionName + " declared")

        # We obtain the head argument variables and the context of the function to execute
        (headArgs, context) = self.functionsDict[functionName]

        # We use the call parameters and the ones in the procedure to initialize the dictionary
        callArgs = arguments[1:]
        if len(headArgs) != len(callArgs):
            self.__errorandexit("Invalid number of arguments in procedure " + functionName)
        for i in range(len(headArgs)):
            self.symbolDict[headArgs[i]] = float(callArgs[i])

        # We visit the context of the desired procedure 
        self.visit(context)
        
        # Final message shown
        if visitor.returning and visitor.valueReturned is not None:
            print("Execution of procedure " + functionName + " finished correctly with returned value " + str(visitor.valueReturned))
        else:
            print("Execution of procedure " + functionName + " finished correctly")
        sys.exit(0)

    # This function only saves the context of all the procedures declared inside the dictionary of procedures.
    # It also inform if there are any errors like redeclaration of procedures or not allowed procedures names.
    def visitProcedure(self, ctx:logo3dParser.ProcedureContext):
        functionName = ctx.VAR().getText()
        headArgs = self.visit(ctx.argumentsHead())
        if functionName in self.functionsDict:
            self.__errorandexit("Redeclaration of procedure " + functionName)
        
        if functionName == "help" or functionName == "show" or functionName == "hide" or functionName == "home" \
        or functionName == "color" or functionName == "forward" or functionName == "backward" \
        or functionName == "up" or functionName == "down" or functionName == "left" or functionName == "right":
            self.__errorandexit("Procedure name " + functionName + " not allowed")
        
        self.functionsDict[functionName] = (headArgs, ctx.statements())

    # This function visits all it's children if no return value is achieved.
    # If we find a return we won't execute any more statement finishing the execution of this function.
    def visitStatements(self, ctx:logo3dParser.StatementsContext):
        childList = list(ctx.getChildren())
        for child in childList:
            if visitor.returning: return
            else: self.visit(child)

    # This function visits an assignment. This assignment can be :=, += or -=
    # so we need to store the starting value in order to change it properly if any 
    # of the two lasts assignments are the chosen ones.
    def visitAssignment(self, ctx:logo3dParser.AssignmentContext):
        valueToAssign = self.visit(ctx.expression())
        startingValue = 0
        if ctx.VAR().getText() in self.symbolDict:
            startingValue = self.symbolDict[ctx.VAR().getText()]

        if ctx.ASSIG() is not None:
            self.symbolDict[ctx.VAR().getText()] = valueToAssign
        elif ctx.ADDASSIG() is not None:
            self.symbolDict[ctx.VAR().getText()] = startingValue + valueToAssign
        else:
            self.symbolDict[ctx.VAR().getText()] = startingValue - valueToAssign

    # This function reads a value and store it in a variable.
    # Remember that variables are local to each context.
    def visitRead(self, ctx:logo3dParser.ReadContext):
        variableName = ctx.VAR().getText()
        self.symbolDict[variableName] = float(input())

    # This function writes in the standard output the desired information.
    # This information can be a STRING, an expression or a simple ENDL.
    def visitWrite(self, ctx:logo3dParser.WriteContext):
        if ctx.STRING() is not None: 
            # I used [1:(len(ctx.STRING().getText())-1)] to delete the " of the STRING
            print(ctx.STRING().getText()[1:(len(ctx.STRING().getText())-1)], end="")
        elif ctx.expression() is not None:
             print(self.visit(ctx.expression()), end="")
        else:
             print()

    # This function visits a conditional. If the valueCondition is cosidered true
    # then we visit the first block of actions. If it's false and we have an else section we will execute it.
    def visitConditional(self, ctx: logo3dParser.ConditionalContext):
        childList = list(ctx.getChildren())
        valueCondition = self.visit(childList[1])
        if (valueCondition < -1e-6) or (valueCondition > 1e-6):
            self.visit(childList[3])
        elif ctx.ELSE() is not None:
            self.visit(childList[5])

    # This functions visits a while loop. It's implemented with a while
    # that checks if the condition is evaluated as true and continues execution the block of code.
    # If after visiting the block we are returning we stop the while and finish this execution.
    # If there is no return statement we will evaluate again the condition and continue executing or not the block of code assigned.
    def visitWhileIt(self, ctx: logo3dParser.WhileItContext):
        childList = list(ctx.getChildren())
        valueCondition = self.visit(childList[1])
        while (valueCondition < -1e-6) or (valueCondition > 1e-6):
            self.visit(childList[3])
            if visitor.returning: return
            valueCondition = self.visit(childList[1])

    # This funcition visits a for loop. It's implemented in a similar way as the previous function.
    # The main change is that we have to evaluate the TO condition and check if our iterating variable
    # is smaller or equal or not. Also we autoincrement the iterative variable by a unit as a classic for.
    def visitForIt(self, ctx: logo3dParser.ForItContext):
        childList = list(ctx.getChildren())
        variableName = childList[1].getText()
        fromExpression = self.visit(childList[3])
        self.symbolDict[variableName] = fromExpression
        while self.symbolDict[variableName] <= self.visit(childList[5]):
            self.visit(childList[7])
            if visitor.returning: return
            self.symbolDict[variableName] += 1 

    # This function visits the implementation of the switch. It delegate the main problem to the visitor visitCases.
    # This part only considers the optional DEFAULT case which will be executed if no break is achieved in the mentioned visitor.
    # It's also important to notice that if a RETURN statment is achieved we will stop the execution of this function without
    # considering evaluating any default case.
    def visitSwitchCase(self, ctx:logo3dParser.SwitchCaseContext):
        childList = list(ctx.getChildren())
        breaked = self.visit(ctx.cases())
        if visitor.returning: return
        if ctx.DEFAULT() is not None:
            if not breaked:
                self.visit(ctx.statements())

    # This function visits the cases of the switch implementation. The main idea is that this context can have many child cases
    # each with a possible BREAK condition and a matching target that will run the case. I will comment around the code to clarify
    # what is doing in every moment.
    def visitCases(self, ctx:logo3dParser.CasesContext):
        # First of all we obtain the expression value that we will try to match with the cases
        value = self.visit(ctx.expression())
        childList = list(ctx.getChildren())
        # We allocate our pointing variable to the first CASE (if exists)
        i = 3 
        # While we are not at the final of the children list
        while i < len(childList):
            # We add 1 to get into the position where the real value is
            i += 1
            # We check if we match this case
            if value == float(childList[i].getText()):
                # If so, we visit the code block of this case
                self.visit(childList[i+1])
                # If there is a return we will stop the execution of this function
                if visitor.returning: return False
                # We add 2 to be in the possible BREAK condition
                i += 2
                # If it didn't exist it means that we arrived to the final case and we can return
                if i >= len(childList): return False
                maybeBreak = childList[i].getText()
                # If the position contains a BREAK we will return true and finish
                # Else it means that in this position we have a new CASE so we will continue executing
                if maybeBreak == "BREAK" or maybeBreak == "break": return True
            else:
                i += 2
                # If it didn't exist it means that we arrived to the final case and we can return
                if i >= len(childList): return False
                maybeBreak = childList[i].getText()
                # If the position contains a BREAK we will skip it by acceding the new case
                # Else it means that in this position we have a new CASE so we will continue executing
                if maybeBreak == "BREAK" or maybeBreak == "break": i += 1
        return False

    # This function visits a function call. It has to save the context of the variables in the dictionary to continue executing
    # when the function call finishes. It also manage all the returning values.
    def visitFunctionCall(self, ctx: logo3dParser.FunctionCallContext):
        # We obtain the function name and the calling args in form of a list.
        functionName = ctx.VAR().getText()
        callArgs = self.visit(ctx.argumentsCall())
        # First we try to match the turtle functions.
        # If it's not matched with a turtle function we search in the dictionary to see if it exist and executed.
        
        # Every possible function turtle is in here. If we match a turtle function but the number of parameters did not match
        # we will show an usage message for the function and stop. Else we will run the turtle function.
        if functionName == "help":
            if len(callArgs) != 0:
                self.__errorandexit("Invalid number of arguments in function " + functionName)
            print(self.turtle.__doc__)

        elif functionName == "show":
            if len(callArgs) != 0:
                print(self.turtle.show.__doc__)
                self.__errorandexit("Invalid number of arguments in function " + functionName)
            self.turtle.show()
            
        elif functionName == "hide":
            if len(callArgs) != 0:
                print(self.turtle.hide.__doc__)
                self.__errorandexit("Invalid number of arguments in function " + functionName)
            self.turtle.hide()
            
        elif functionName == "home":
            if len(callArgs) != 0:
                print(self.turtle.home.__doc__)
                self.__errorandexit("Invalid number of arguments in function " + functionName)
            self.turtle.home()
            
        elif functionName == "color":
            if len(callArgs) != 3:
                print(self.turtle.color.__doc__)
                self.__errorandexit("Invalid number of arguments in function " + functionName)
            # This if calls the function color_aux of the turle
            if not self.turtle.color_aux(callArgs):
                print(self.turtle.color.__doc__)
                self.__errorandexit("Parameter out of range in function " + functionName)
                
        elif functionName == "forward":
            if len(callArgs) != 1:
                print(self.turtle.forward.__doc__)
                self.__errorandexit("Invalid number of arguments in function " + functionName)
            self.turtle.forward(callArgs[0])
            
        elif functionName == "backward":
            if len(callArgs) != 1:
                print(self.turtle.backward.__doc__)
                self.__errorandexit("Invalid number of arguments in function " + functionName)
            self.turtle.backward(callArgs[0])
            
        elif functionName == "up":
            if len(callArgs) != 1:
                print(self.turtle.up.__doc__)
                self.__errorandexit("Invalid number of arguments in function " + functionName)
            self.turtle.up(callArgs[0])
            
        elif functionName == "down":
            if len(callArgs) != 1:
                print(self.turtle.down.__doc__)
                self.__errorandexit("Invalid number of arguments in function " + functionName)
            self.turtle.down(callArgs[0])
            
        elif functionName == "left":
            if len(callArgs) != 1:
                print(self.turtle.left.__doc__)
                self.__errorandexit("Invalid number of arguments in function " + functionName)
            self.turtle.left(callArgs[0])
            
        elif functionName == "right":
            if len(callArgs) != 1:
                print(self.turtle.right.__doc__)
                self.__errorandexit("Invalid number of arguments in function " + functionName)
            self.turtle.right(callArgs[0])
            
        else:
            # If it's not a turtle function we search for it in the function dictionary throwing an error if the
            # desired function is not declared.
            if functionName not in self.functionsDict:
                self.__errorandexit("No procedure " + functionName + " declared")

            # We obtain the head argument variables and the context of the procedure to execute
            (headArgs, newContext) = self.functionsDict[functionName]

            # Add to the top of the stack the current dictionary
            self.symbolStack.append(self.symbolDict)

            # We use the call parameters and the ones in the procedure (headArgs) to initialize the dictionary
            self.symbolDict = {}
            if len(headArgs) != len(callArgs):
                self.__errorandexit("Invalid number of arguments in procedure " + functionName)
            for i in range(len(headArgs)):
                self.symbolDict[headArgs[i]] = callArgs[i]

            # We visit the context of the desired procedure
            self.visit(newContext)
            
            # As we finish the procedure call we pop the previous context dictionary
            self.symbolDict = self.symbolStack.pop()
            
            # If we are returning a value we reset these indicators and return the value that is being returned
            returnedValue = None
            if visitor.returning:
                returnedValue = visitor.valueReturned
                visitor.returning = False
                visitor.valueReturned = None
            
            return returnedValue
    
    # This function manages the visit of a return statement.
    # It simply visits the expression (if it exists) and set the boolean returning to true to indicate that we are returning a value
    # which will be saved in the variable valueReturned. If we are returning void we will let valueReturned to maintain as None.
    def visitReturnStatement(self, ctx:logo3dParser.ReturnStatementContext):
        visitor.valueReturned = None
        if ctx.expression() is not None:
            visitor.valueReturned = self.visit(ctx.expression())
        visitor.returning = True

    # This function visits a paranthesized expression which only returns the value of the expression inside the parenthesis.
    def visitParenthesisExpression(self, ctx: logo3dParser.ParenthesisExpressionContext):
        return self.visit(ctx.expression())

    # This function visits a unary expression + or -.
    # If it's negative we invert the sign of the value returned.
    # Nothing has to be done with the ADD symbol.
    def visitUnaryExpression(self, ctx:logo3dParser.UnaryExpressionContext):
        value = self.visit(ctx.expression())
        if ctx.SUB() is not None: value = -value
        return value

    # This function visits the auto-increment or auto-decrement operators.
    # These operators needs the previous value of the variable so we saved in the variable value (0 if is not already used).
    # Then we increment or decrement the dictionary with the correct value and also return it.
    def visitAutoExpression(self, ctx:logo3dParser.AutoExpressionContext):
        value = 0
        if ctx.VAR().getText() in self.symbolDict:
            value = self.symbolDict[ctx.VAR().getText()]

        if ctx.INC() is not None:
            self.symbolDict[ctx.VAR().getText()] = value + 1
            return value + 1
        else:
            self.symbolDict[ctx.VAR().getText()] = value - 1
            return value - 1

    # This function visits a high presedence arithmetic operation.
    # It's quite simple to follow; we return the value resulting in the corresponding operation.
    # Nevertheless, I control that non division, integer division nor modulo are operated with 0 in the right position.
    def visitArithmeticExpressionHigh(self, ctx: logo3dParser.ArithmeticExpressionHighContext):
        childList = list(ctx.getChildren())
        ariOp = childList[1].getText()
        left = self.visit(childList[0])
        right = self.visit(childList[2])

        if ariOp == "*":
            return left * right
        else:
            if right == 0:
                self.__errorandexit("Divison by zero in " + ctx.getText())
            if ariOp == "/":
                return left / right
            elif ariOp == "%":
                return left % right
            else:
                # This is the integer division in python
                return left // right

    # This function visits a low presedence arithmetic operation.
    # It's quite simple to follow; we return the value resulting in the corresponding operation.
    def visitArithmeticExpressionLow(self, ctx:logo3dParser.ArithmeticExpressionLowContext):
        childList = list(ctx.getChildren())
        ariOp = childList[1].getText()
        left = self.visit(childList[0])
        right = self.visit(childList[2])

        if ariOp == "+":
            return left + right
        else:
            return left - right

    # This function visits a relation operation.
    # It's quite simple to follow; we return 1 (true) if the relation of the operands is correct (true)
    # and we return 0 (false) otherwise, when the relation is not correct.
    def visitRelationalExpression(self, ctx: logo3dParser.RelationalExpressionContext):
        childList = list(ctx.getChildren())
        relOp = self.visit(childList[1])
        left = self.visit(childList[0])
        right = self.visit(childList[2])

        if relOp == "==":
            if left == right: return 1
        elif relOp == "!=":
            if left != right: return 1
        elif relOp == "<":
            if left < right: return 1
        elif relOp == ">":
            if left > right: return 1
        elif relOp == "<=":
            if left <= right: return 1
        elif relOp == ">=":
            if left >= right: return 1
        return 0

    # Function that returns the text (e.g. "<", "==", ...) of a relational operator
    def visitRelationalOP(self, ctx: logo3dParser.RelationalOPContext):
        return ctx.getText()

    # This function visits a logical NOT expression.
    # It will return 0.0 (false) whenever the value is true and 1.0 true otherwise.
    # (i.e. It will return the negation, in terms of booleans, of the expression to consider)
    def visitLogicalNotExpression(self, ctx:logo3dParser.LogicalNotExpressionContext):
        value = self.visit(ctx.expression())
        if (value < -1e-6) or (value > 1e-6): return 0.0
        else: return 1.0
    
    # This function visits a logical AND expression.
    # It will return 1.0 (true) when both expressions are cosidered true and false otherwise.
    # It behaves like a normal logical and.
    def visitLogicalAndExpression(self, ctx:logo3dParser.LogicalAndExpressionContext):
        childList = list(ctx.getChildren())
        leftValue = self.visit(childList[0])
        rightValue = self.visit(childList[2])
        if (leftValue < -1e-6) or (leftValue > 1e-6):
            if (rightValue < -1e-6) or (rightValue > 1e-6):
                return 1.0
        return 0.0
    
    # This function visits a logical OR expression.
    # It will return 0.0 (false) when both expressions are cosidered false and true otherwise.
    # It behaves like a normal logical or.
    def visitLogicalOrExpression(self, ctx:logo3dParser.LogicalOrExpressionContext):
        childList = list(ctx.getChildren())
        leftValue = self.visit(childList[0])
        rightValue = self.visit(childList[2])
        if (leftValue > -1e-6) and (leftValue < 1e-6):
            if (rightValue > -1e-6) and (rightValue < 1e-6):
                return 0.0
        return 1.0
    
    # This function visits a number expression which just return the float value of the text associated.
    def visitNumberExpression(self, ctx: logo3dParser.NumberExpressionContext):
        return float(ctx.getText())

    # This function visits a variable expression which just return the value of a variable searching in the
    # variable dictionary. If it didn't exist there we will return 0.0 as a default value.
    def visitVariableExpression(self, ctx: logo3dParser.VariableExpressionContext):
        if ctx.getText() in self.symbolDict:
            return self.symbolDict[ctx.getText()]
        return 0.0

    # This function visits a function call expression which requires the function to return a value.
    # If will visit the function call expecting it to receive a returned value. If the function called
    # didn't return any value we will throw an error informing about the situation.
    def visitFunctionExpression(self, ctx:logo3dParser.FunctionExpressionContext):
        returnedValue = self.visit(ctx.functionCall())
        if returnedValue is None:
            self.__errorandexit("Return value required for procedure call " + ctx.getText().split()[0])
        return returnedValue
    
    # This function visits the arguments of the head of a procedure.
    # This type of arguments can only be variables so we will just return a list with all the variables
    # skipping all the commas that will be in the middle.
    def visitArgumentsHead(self, ctx: logo3dParser.ArgumentsHeadContext):
        childList = []
        for arg in list(ctx.getChildren()):
            argText = arg.getText()
            if argText != ',':
                childList.append(argText)
        return childList

    # This function visits the arguments of the call of a procedure.
    # This type of arguments can be any kind of expressionso we will just return a list with all these
    # expressions evaluated skipping all the commas that will be in the middle.
    def visitArgumentsCall(self, ctx: logo3dParser.ArgumentsCallContext):
        childList = []
        for arg in list(ctx.getChildren()):
            argText = arg.getText()
            if argText != ',':
                childList.append(self.visit(arg))
        return childList