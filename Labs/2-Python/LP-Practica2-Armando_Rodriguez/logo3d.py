import sys
from antlr4 import *
from logo3dLexer import logo3dLexer
from logo3dParser import logo3dParser
from visitor import visitor

# Read the programm
input_stream = FileStream(sys.argv[1])

# Parse the programm
lexer = logo3dLexer(input_stream)
token_stream = CommonTokenStream(lexer)
parser = logo3dParser(token_stream)
tree = parser.root()

# We visit the tree which will visit all the 
# procedures and save their corresponding context
myVisitor = visitor()
myVisitor.visit(tree)

# Change the recursion limit to 10000 to don't have any interference
sys.setrecursionlimit(10000)

# If we don't indicate any function we will run the procedure main using the function run
# Else we will send all the list with the function name and the parameters to the function run
if len(sys.argv[2:]) == 0:
    myVisitor.run(["main"])
else: 
    myVisitor.run(sys.argv[2:])