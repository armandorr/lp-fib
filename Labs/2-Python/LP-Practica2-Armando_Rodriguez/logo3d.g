grammar logo3d;

PROC   : 'PROC'   | 'proc'   ;
IS     : 'IS'     | 'is'     ;
RETURN : 'RETURN' | 'return' ;

IF   : 'IF'   | 'if'   ;
THEN : 'THEN' | 'then' ;
ELSE : 'ELSE' | 'else' ;
END  : 'END'  | 'end'  ;

WHILE : 'WHILE' | 'while' ;
DO    : 'DO'    | 'do'    ;

FOR  : 'FOR'  | 'for'  ;
FROM : 'FROM' | 'from' ;
TO   : 'TO'   | 'to'   ;

SWITCH  : 'SWITCH'  | 'switch'  ;
CASE    : 'CASE'    | 'case'    ;
BREAK   : 'BREAK'   | 'break'   ;
DEFAULT : 'DEFAULT' | 'default' ;

NOT : 'NOT' | 'not' ;
AND : 'AND' | 'and' ;
OR  : 'OR'  | 'or'  ;

ASSIG    : ':='  ;
ADDASSIG : '+=' ;
SUBASSIG : '-=' ;

READ_SYMBOL  : '>>' ;
WRITE_SYMBOL : '<<' ;

ENDL : 'ENDL' | 'endl' ;

ADD  : '+'   ;
SUB  : '-'   ;
MUL  : '*'   ;
DIV  : '/'   ;
MOD  : '%'   ;
FDIV : '\\'  ;
INC  : '++'  ;
DEC  : '--'  ;

EQ  : '==' ;
NEQ : '!=' ;
LT  : '<'  ;
GT  : '>'  ;
LTE : '<=' ;
GTE : '>=' ;

DOT    : '.' ;
COMMA  : ',' ;
LPAREN : '(' ;
RPAREN : ')' ;

STRING : '"' ( ~[\\"\r\n] )* '"';

VAR : [a-zA-Z_]+ [a-zA-Z0-9_]* ;

REAL : ([0-9]* DOT [0-9]+) | [0-9]+ ;

root : (procedure)* EOF ;

procedure : PROC VAR LPAREN argumentsHead RPAREN IS statements END ;

statements : (statement)* ;

statement :
      assignment     
    | read      
    | write    
    | conditional  
    | whileIt     
    | forIt
    | switchCase
    | functionCall
    | returnStatement
    ;

assignment: VAR (ASSIG | ADDASSIG | SUBASSIG)  expression;

read : READ_SYMBOL VAR ;

write : WRITE_SYMBOL (expression | STRING | ENDL) ;

conditional : IF expression THEN statements (ELSE statements)? END ;

whileIt : WHILE expression DO statements END ;

forIt : FOR VAR FROM expression TO expression DO statements END ;

switchCase : SWITCH cases (DEFAULT statements)? END ;

cases : LPAREN expression RPAREN (CASE REAL statements BREAK?)* ;

functionCall : VAR LPAREN argumentsCall RPAREN ;
   
returnStatement : RETURN (expression)?;

expression :
      LPAREN expression RPAREN                         #parenthesisExpression
    | (ADD|SUB) expression                             #unaryExpression
    | (INC|DEC) VAR                                    #autoExpression
    | expression (MUL | DIV | MOD | FDIV) expression   #arithmeticExpressionHigh
    | expression (ADD | SUB) expression                #arithmeticExpressionLow
    | expression relationalOP expression               #relationalExpression
    | NOT expression                                   #logicalNotExpression
    | expression AND expression                        #logicalAndExpression
    | expression OR expression                         #logicalOrExpression
    | REAL                                             #numberExpression
    | VAR                                              #variableExpression
    | functionCall                                     #functionExpression
    ;

relationalOP :
      EQ
    | NEQ 
    | GT
    | GTE
    | LT
    | LTE
    ;
    
argumentsHead : (VAR (COMMA VAR)*)? ;

argumentsCall : (expression (COMMA expression)*)? ;

LINE_COMMENT : '//' ~[\r\n]* -> skip ;

BLOCK_COMMENT : '/*' .*? '*/' -> skip ;
    
WS : [ \n\r\t]+ -> skip ;