from vpython import *
import math


class Turtle3D:
    """
    Help on module turtle3D:

    NAME:
        turtle3D

    DESCRIPTION:
        This module provides a set of instructions that allow to draw in a 3D space

    FUNCTIONS:
        forward(x):
            Function that moves the turtle x units forward.

        backward(x):
            Function that moves the turtle x units forward.

        right(x):
            Function that moves x degrees the looking direction of the turtle to the right.

        left(x):
            Function that moves x degrees the looking direction of the turtle to the left.

        up(x):
            Function that moves x degrees the looking direction of the turtle up.

        down(x):
            Function that moves x degrees the looking direction of the turtle down.

        color(r,g,b):
            Function that changes the color of the turtle with the new r g b given.

        hide():
            Function that hides the turtle.

        show():
            Function that make the turtle visible.

        home():
            Function that moves the turtle to it's home coordinates.
    """
    __homeCoordinates = vector(0, 0, 0)
    __sphereRadius = 0.1
    __cylinderRadius = 0.1

    def __init__(self):
        """
        Turtle3D default constructor.

        This constructor initialize all these parameters with default values.
            coordinates : Turtle coordinates (vector)
            horizontalAngle: The turtle horizontal angle in radians
            verticalAngle: The turtle vertical angle in radians
            painting: Indicates if the turtle should paint (True) or not (False)

        It also initialize the scene where we are going to paint.
        """
        self.__coordinates = Turtle3D.__homeCoordinates
        self.__horizontalAngle = self.__verticalAngle = 0
        self.__turtleColor = vector(1, 0, 0)
        self.__painting = True
        self.__turtleScene = scene
        self.__initScene()

    def __initScene(self):
        """
        Scene initializer.

        This private function initialize the scene with default values.
        """
        self.__turtleScene.height = scene.width = 1000
        self.__turtleScene.autocenter = True
        self.__turtleScene.caption = """\nTo rotate "camera", drag with right button or Ctrl-drag.\nTo zoom, drag with middle button or Alt/Option depressed, or use scroll wheel. On a two-button mouse, middle is left + right.\nTo pan left/right and up/down, Shift-drag. Touch screen: pinch/extend to zoom, swipe or two-finger rotate.\n\n"""

    def __paintSphere(self):
        """
        Sphere painter.

        This private function paints a sphere in the position of the implicit turtle with it's defined color.
        The radius used is the radius of the class Turtle3D.
        """
        sphere(pos=self.__coordinates, radius=Turtle3D.__sphereRadius, color=self.__turtleColor)

    def __vectFromAngles(self):
        """
        Vector from turtle angles getter.

        This private function returns the unitary vector calculated from the turtle angles.
        """
        x = math.cos(self.__horizontalAngle) * math.cos(self.__verticalAngle)
        y = math.sin(self.__verticalAngle)
        z = math.sin(self.__horizontalAngle) * math.cos(self.__verticalAngle)
        return vector(x, y, z)

    def __paintPath(self, translationVector):
        """
        Path painter.

        This private function paints a path from the implicit turtle coordinates towards the parameter
        vector (translationVector) using the color of the implicit turtle.
        A path consists into two spheres and one cylinder (with a defined radius) joining them.
        Then we will modify the turtle coordinates to set the implicit turtle at the final of the path.

        Even though, this won't happen if the turtle is not painting (self.__painting != True).
        In this case the function only modify the coordinates without painting the path.
        """
        if self.__painting:
            self.__paintSphere()
            cylinder(pos=self.__coordinates, axis=translationVector, radius=Turtle3D.__cylinderRadius, color=self.__turtleColor)
            self.__coordinates += translationVector
            self.__paintSphere()
        else:
            self.__coordinates += translationVector

    def forward(self, x):
        """
        Help on forward function:

        USAGE:
            forward(x)

            Function that given a distance x moves the turtle x units forward.

            Args:
                x: The distance to move the turtle forward.

            Forward is the direction where the implicit turtle is looking at.
        """
        translationVector = x * self.__vectFromAngles()
        self.__paintPath(translationVector)

    def backward(self, x):
        """
        Help on backward function:

        USAGE:
            backward(x)

            Function that given a distance x moves the turtle x units backward.

            Args:
                x: The distance to move the turtle backward.

            Backward is the opposite direction where the implicit turtle is looking at.
        """
        translationVector = -x * self.__vectFromAngles()
        self.__paintPath(translationVector)

    def color_aux(self, rgb):
        """
        Function that changes the color of the turtle.

        Args:
            rgb: The list that contains the new color.

        Return:
            This function returns False if any of the parameters of the list is incorrect.
            Returns true otherwise and call the function color to change the color.
        """
        r, g, b = rgb[0], rgb[1], rgb[2]
        if r < 0 or r > 1 or g < 0 or g > 1 or b < 0 or b > 1:
            return False
        self.color(r, g, b)
        return True

    def color(self, r, g, b):
        """
        Help on color function:

        USAGE:
            color(r,g,b)

            Function that changes the color of the implicit turtle.

            Args:
                r: The color red component of the new color in range 0 to 1.
                g: The color green component of the new color in range 0 to 1.
                b: The color blue component of the new color in range 0 to 1.

            This function changes the painting color of the implicit turtle making the r g b arguments the new color.
        """
        self.__turtleColor = vector(r, g, b)

    def hide(self):
        """
        Help on hide function:

        USAGE:
            hide()

            Function that hides the implicit turtle.

            This function makes the turtle stop painting it's trace.
        """
        self.__painting = False

    def show(self):
        """
        Help on hide function:

        USAGE:
            show()

            Function that shows the implicit turtle.

            This function makes the turtle visible when painting it's trace.
        """
        self.__painting = True

    def home(self):
        """
        Help on home function:

        USAGE:
            home()

            Function that moves the turtle to it's home.

            This function restores the default coordinates of the implicit turtle.
            The turtle will be placed in the home coordinates after this function.
        """
        self.__coordinates = Turtle3D.__homeCoordinates

    def right(self, x):
        """
        Help on right function:

        USAGE:
            right(x)

            Function that moves x degrees the looking direction of the implicit turtle to the right.

            Args:
                x: The angle in degrees to move the looking direction of the turtle.

            This function changes the direction where the turtle is looking at by moving this direction
            to the right x degrees.
        """
        self.__horizontalAngle += math.radians(x)

    def left(self, x):
        """
        Help on left function:

        USAGE:
            left(x)

            Function that moves x degrees the looking direction of the implicit turtle to the left.

            Args:
                x: The angle in degrees to move the looking direction of the turtle.

            This function changes the direction where the turtle is looking at by moving this direction
            to the left x degrees.
        """
        self.__horizontalAngle -= math.radians(x)

    def up(self, x):
        """
        Help on up function:

        USAGE:
            up(x)

            Function that moves x degrees the looking direction of the implicit turtle up.

            Args:
                x: The angle in degrees to move the looking direction of the turtle.

            This function changes the direction where the turtle is looking at by moving this direction up x degrees.
        """
        self.__verticalAngle += math.radians(x)

    def down(self, x):
        """
        Help on down function:

        USAGE:
            down(x)

            Function that moves x degrees the looking direction of the implicit turtle down.

            Args:
                x: The angle in degrees to move the looking direction of the turtle.

            This function changes the direction where the turtle is looking at by moving this direction down x degrees.
        """
        self.__verticalAngle -= math.radians(x)